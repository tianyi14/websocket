package com.ps.websocket.websocket;

import com.alibaba.fastjson.JSONObject;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;


@Slf4j
@Component
@ServerEndpoint(value = "/testWebsocket/{name}")
public class YmWebSocketEndPoint {

    private static final String PREFIX_EMPLOYEE_SESSION = "prefix:employee:session:";

    private String employeeId;
    private static AtomicInteger onlineCount = new AtomicInteger(0);
    private static ConcurrentHashMap<String, Session> employeeId2Session = new ConcurrentHashMap<>();



    @OnOpen
    public void onOpen(Session session, @PathParam("name") String name) throws IOException {
        this.employeeId = name;
        addOnlineCount();
        employeeId2Session.put(PREFIX_EMPLOYEE_SESSION + employeeId, session);
        //记录客服ws 在哪台服务器  分布式情况下使用
       // String hostAddress = InetAddress.getLocalHost().getHostAddress();
        //JedisUtils.set(Global.REDIS_PREFIX + "ONLINE_SESSION_CUSTOMER_ID:" + employeeId, hostAddress, 0);



        log.info("用户：{}的客服上线!当前在线人数：{}", employeeId, getOnlineCount());
    }

    @OnMessage
    public String onMessage(String message) throws IOException {
        log.info("收到来自客户端：{}，消息：{}", employeeId, message);
        return message;
    }

    @OnClose
    public void onClose() throws IOException {
        employeeId2Session.remove(PREFIX_EMPLOYEE_SESSION + employeeId);
        reduceOnlineCount(); // 在线数减1
        //JedisUtils.del(Global.REDIS_PREFIX + "ONLINE_SESSION_CUSTOMER_ID:" + employeeId);
        log.info("有一个用户{}下线！当前在线连接数:{}", employeeId, getOnlineCount());

    }


    /**
     * 发生错误时调用
     */
    @OnError
    public void onError(Session session, Throwable error) {
       // JedisUtils.del(Global.REDIS_PREFIX + "ONLINE_SESSION_CUSTOMER_ID:" + employeeId);
        log.error("ymWebSocket 发生错误", error);
    }

    /**
     * 服务端给客户端发送消息
     */
    public String sendMessage(String message) throws IOException {
        JSONObject object = JSONObject.parseObject(message);
        Object userId = object.get("userId");
        Session session = employeeId2Session.get("prefix:employee:session:" + userId.toString());
        if (session != null) {
            synchronized(session) {
                log.info("socket服务端给客户端发送消息");
                session.getBasicRemote().sendText(message);
            }
        }
        //多机环境 这里拿不到session 去找当前session 存储哪台服务器 然后去调相对应服务器的接口
        return message;
    }





    public static void addOnlineCount() {
        onlineCount.getAndIncrement();
    }

    public static int getOnlineCount() {
        return onlineCount.get();
    }

    public static void reduceOnlineCount() {
        onlineCount.getAndDecrement();
    }

}