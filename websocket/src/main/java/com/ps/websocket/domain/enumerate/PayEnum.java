package com.ps.websocket.domain.enumerate;

import com.fasterxml.jackson.annotation.JsonCreator;

//支付枚举
public enum PayEnum {

    WX_PAY(0, "微信app支付"),

    ALI_PAY(1, "支付宝app支付");




    private int code;

    private String name;

    PayEnum(int code, String name) {

        this.code = code;

        this.name = name;

    }

    @JsonCreator
    public static PayEnum getEnum(int code) {
        for(PayEnum s:PayEnum.class.getEnumConstants()) {
            if(s.code == code) {
                return s;
            }
        }
        return null;
    }

}
