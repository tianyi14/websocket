package com.ps.websocket.domain;

import lombok.Data;

@Data
public class AmountInfo {

    private Integer total;

    private String currency;

}
