package com.ps.websocket.domain.enumerate.converter;

import com.ps.websocket.domain.enumerate.PayEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class PayEnumConverter implements Converter<Integer, PayEnum> {

    @Override
    public PayEnum convert(Integer source) {
        if (source == null) {
            return null;
        }


        return PayEnum.getEnum(source);
    }
}

