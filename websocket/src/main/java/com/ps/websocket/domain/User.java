package com.ps.websocket.domain;

import lombok.Data;
import org.springframework.data.elasticsearch.annotations.Document;

@Data
@Document(indexName = "peishuai",type = "peishuai_type")
public class User {

    private Integer userId;

    private String name;

    private Integer geneder;
}
