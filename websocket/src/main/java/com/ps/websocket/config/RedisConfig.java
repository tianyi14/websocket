package com.ps.websocket.config;

import com.ps.websocket.config.bean.RedisConfigBean;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import redis.clients.jedis.JedisPool;

@Configuration
public class RedisConfig {

    @Autowired
    private RedisConfigBean redisConfigBean;




    @Bean
    @DependsOn("redisConfigBean")
    public JedisPool jedisPool(){
        GenericObjectPoolConfig genericObjectPoolConfig = new GenericObjectPoolConfig();
        genericObjectPoolConfig.setMinIdle(redisConfigBean.getMinIdle());
        genericObjectPoolConfig.setMaxIdle(redisConfigBean.getMaxIdle());
        genericObjectPoolConfig.setMaxTotal(redisConfigBean.getMaxTotal());
        genericObjectPoolConfig.setMaxWaitMillis(redisConfigBean.getMaxWait());
        JedisPool jedisPool = new JedisPool(genericObjectPoolConfig,redisConfigBean.getHost(),redisConfigBean.getPort(),redisConfigBean.getTimeout());


        return jedisPool;
    }



}
