package com.ps.websocket.config;


import com.ps.websocket.domain.enumerate.PayEnum;
import com.ps.websocket.service.PayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class FactoryConfiguration {


    @Autowired
    private List<PayService> payServiceList;


    @Bean("payServiceMap")
    public Map<PayEnum,PayService> payServiceMap(){
        Map<PayEnum,PayService> map = new HashMap<>();
        payServiceList.stream().forEach(p -> {
            map.put(p.getKey(),p);
        });
        return map;
    }
}
