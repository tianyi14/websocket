package com.ps.websocket.config;

import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class CloudConfiguration {



    @Bean
    public HttpClient httpClient() {
        CloseableHttpClient httpclient = HttpClients.custom()
                .setDefaultRequestConfig(requestConfig())
                .setConnectionManager(httpClientConnectionManager())
                .build();
        return httpclient;
    }

    @Bean(destroyMethod="shutdown")
    public HttpClientConnectionManager httpClientConnectionManager() {
        PoolingHttpClientConnectionManager httpClientConnectionManager = new PoolingHttpClientConnectionManager();
        httpClientConnectionManager.setMaxTotal(1000);
        httpClientConnectionManager.setDefaultMaxPerRoute(500);

        return httpClientConnectionManager;
    }

    @Bean
    public RequestConfig requestConfig() {
        RequestConfig defaultRequestConfig = RequestConfig.custom()
                .setSocketTimeout(120000)
                .setConnectTimeout(30000)
                .setConnectionRequestTimeout(30000)
                .setCookieSpec(CookieSpecs.IGNORE_COOKIES)
                .build();

        return defaultRequestConfig;
    }
}