package com.ps.websocket.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DirectRabbitConfig {

  /*  @Bean
    public Queue testDirectQueue(){
        // durable:是否持久化,默认是false,持久化队列：会被存储在磁盘上，当消息 宕机重启时仍然存在 可以在后台管理平台path看到要恢复的文件地址
        // exclusive:默认也是false，只能被当前创建的连接使用，而且当连接关闭后队列即被删除。此参考优先级高于durable
        // autoDelete:是否自动删除，当没有生产者或者消费者使用此队列，该队列会自动删除

        //一般设置一下队列的持久化就好,其余两个就是默认false
        new Queue("",false);
        return QueueBuilder
                .durable("testDirectQueue")
                //声明该队列的死信消息发送到的 交换机 （队列添加了这个参数之后会自动与该交换机绑定，并设置路由键，不需要开发者手动设置)
                .withArgument("x-dead-letter-exchange", "testDeadDirectExchange")
                //声明该队列死信消息在交换机的 路由键
                .withArgument("x-dead-letter-routing-key", "dead-letter-routing-key")
                .build();

    }
    @Bean
    public Queue testDirectQueue2(){

        return QueueBuilder
                .durable("testDirectQueue2")
                .build();

    }


    //死信队列  死信队列/交换机 在mq后台管理平台也可手动去绑定
    @Bean
    public Queue testDeadDirectQueue(){

        return QueueBuilder
                .durable("testDeadDirectQueue")
                .build();
    }

    //direct交换机
    @Bean
    public DirectExchange testDirectExchange(){

        return new DirectExchange("testDirectExchange",true,false);
    }

    //direct交换机
    @Bean
    public DirectExchange testDirectExchange2(){

        return new DirectExchange("testDirectExchange2",true,false);
    }


    //direct  死信交换机
    @Bean
    public DirectExchange testDeadDirectExchange(){

        return new DirectExchange("testDeadDirectExchange",true,false);
    }


    //绑定 队列 交换机绑定  路由键
    @Bean
    public Binding bindingDirect(){
        return BindingBuilder.bind(testDirectQueue()).to(testDirectExchange()).with("testDirectRouting");
    }

    //绑定 队列 交换机绑定  路由键
    @Bean
    public Binding bindingDirect2(){
        return BindingBuilder.bind(testDirectQueue2()).to(testDirectExchange2()).with("testDirectRouting2");
    }


    //绑定 死信队列 交换机绑定  路由键
    @Bean
    public Binding bindingDeadDirect(){
        return BindingBuilder.bind(testDeadDirectQueue()).to(testDeadDirectExchange()).with("dead-letter-routing-key");
    }


    //测试错误队列 do 回调测试
    @Bean
    public DirectExchange testErrorDirectExchange(){
        return new DirectExchange("testErrorDirectExchange",true,false);
    }

  */

}
