package com.ps.websocket.config;


import com.alipay.api.AlipayApiException;
import com.alipay.api.CertAlipayRequest;
import com.alipay.api.DefaultAlipayClient;
import com.ps.websocket.constant.PayConstants;
import com.ps.websocket.util.EnvironmentUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PayConfiguration {

    //生成 支付宝公钥证书模式 sdk客户端实例
    @Bean("alipayClient")
    public DefaultAlipayClient alipayClient() {
        CertAlipayRequest certAlipayRequest= new CertAlipayRequest ();
        certAlipayRequest.setServerUrl(PayConstants.ALI_GATEWAY_DOMAIN);
        certAlipayRequest.setAppId(PayConstants.ALI_APPID);
        certAlipayRequest.setPrivateKey(PayConstants.ALI_APP_PRIVATE_KEY);
        certAlipayRequest.setFormat("JSON");
        certAlipayRequest.setCharset("UTF-8");
        certAlipayRequest.setSignType("RSA2");
        certAlipayRequest.setCertPath(EnvironmentUtils.getString("alipay.app.cert.public.location"));
        certAlipayRequest.setAlipayPublicCertPath(EnvironmentUtils.getString("alipay.cert.public.location"));
        certAlipayRequest.setRootCertPath(EnvironmentUtils.getString("alipay.root.cert.location"));
        DefaultAlipayClient alipayClient = null;
       /* try {
            //先注释
            alipayClient = new DefaultAlipayClient(certAlipayRequest);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }*/

        return null;
    }

    /*public static void main(String[] args) throws AlipayApiException {
        String publicKey = AlipaySignature.getAlipayPublicKey("D:/aliconfig/app_cert_publickey/appCertPublicKey_2021002134679748.crt");
        System.out.println("应用公钥数据："+publicKey);
    }*/
 }
