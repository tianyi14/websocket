package com.ps.websocket.config.bean;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "spring.redis")
@Data
public class RedisConfigBean {



    private Integer maxActive;

    private Integer maxWait;

    private Integer maxIdle;

    private Integer maxTotal;


    private Integer minIdle;



    private String host;
    private String password;
    private Integer port;
    private Integer index;

    private Integer timeout;
}
