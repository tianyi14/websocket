package com.ps.websocket.constant;

import java.security.PrivateKey;

public class PayConstants {



    // 微信支付appid
    public static String wxAppid ="wxb6e6199df3476e3b";

    // 微信支付商户ID
    public static String wxMchid ="1609112146";

    // 微信支付回调  TODO:等到服务器上后这里需要改
    public static String wxNotifyurl ="https://pay.quwentxw.com/pay/payNotify";

    // 微信支付AppSecret
    public static String wxAppSecret ="";

    // 微信支付  应用秘钥v3
    public static String wxSecretKey ="chenglianhuchuangweixinpay654321";

    // 微信包名
    public static String wxPkg ="";

    //微信平台证书序列号
    public static String serialNo = "";

    //微信 商户证书 私钥
    public static PrivateKey certKey = null;

    //微信支付域名
    public static String WX_PAY_DOMAIN_URL = "https://api.mch.weixin.qq.com";

    //微信app下单接口
    public static String WX_APP_CREATE_ORDER_URL="/v3/pay/transactions/app";

    //微信 v3查询订单接口
    public static String WX_APP_QUERY_ORDER_URL="/v3/pay/transactions/out-trade-no/%s";

    //微信v3 获取平台证书接口
    public static String WX_CERTIFICATES_URL="/v3/certificates";

    //微信 签名头信息
    public static String WX_SIGN_HEADER_INFO ="WECHATPAY2-SHA256-RSA2048 ";





    /*
     * 支付宝 支付配置
     * */
    //应用公钥
    public static String ALI_APP_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmqF1UFyR2HlfjCWgev0b61t2+TgkM76l1k74lOWcDz9uWLwY3E+DAjwLcmnlMFDU0P7BrvyPSsvbSXqP8ipQYcgIev4JyexZWImUm3UAXqSA82n9rS7Vf8AzD1yq0eJqTZNXkBuUQKOpBnEDptr7tO9wwHn8wBVNK8AhtAtYyGIFZOBeVgzrF7G4NqRQqxO0ApZArefsIXt3K+QEJKXYL1LFyR/zE5qiH1zgV22F845M0ygpoytuTwUBCEbWxV9pWQjZ7UiyvWnquEUVjYYhQykFmg0nnPB1GRv979sCHL+suryKOtwpigW60dwqZ4pS64/g0bNHpPdojKMnsdpMAwIDAQAB";


    public static String ALI_APP_PRIVATE_KEY = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCaoXVQXJHYeV+MJaB6/RvrW3b5OCQzvqXWTviU5ZwPP25YvBjcT4MCPAtyaeUwUNTQ/sGu/I9Ky9tJeo/yKlBhyAh6/gnJ7FlYiZSbdQBepIDzaf2tLtV/wDMPXKrR4mpNk1eQG5RAo6kGcQOm2vu073DAefzAFU0rwCG0C1jIYgVk4F5WDOsXsbg2pFCrE7QClkCt5+whe3cr5AQkpdgvUsXJH/MTmqIfXOBXbYXzjkzTKCmjK25PBQEIRtbFX2lZCNntSLK9aeq4RRWNhiFDKQWaDSec8HUZG/3v2wIcv6y6vIo63CmKBbrR3CpnilLrj+DRs0ek92iMoyex2kwDAgMBAAECggEALF+/IbOe6udRww8dMY4nD/BsgPJaeTrxrkF1Vo1Zrop+aRp5a+epad+/TTMUYTwhBmTFQikaGdXMdRal7lG0RYs428rBFmAr95truPzngUFl+/Rjpuo0vHd0b6khICAAb4ij7Sfs8GvG2qFd8ekPqIm56pJXNOwn/DEjut4yuTXriOe8ClMf0c0gLENPgfWOY87zNrTQ/OP+CkevdY9sJQNfU9r0vLiwvoqQ7qmZEQgevAqJzKNSU3Bt+avlI3mRyRvuDIlMie6GjdL7aUlQigoV7XIdCUZNovmb8ESm28eKD5RCfVYuJA5eB/32Hh0fJq+s/z+5HcCsirOxD/lSsQKBgQDxqBqOmiJgFsRzLkNj67qUZPsdibrrH8jh43AjzUREfGJ8Risu2GiSTErpitIhPDEmUvrevZxoWbbMQ+C1UOERuf2R1WNpGcUkrHrKiBPVvFYMPWsD2yC3EkXq1wleRCpN/NdoKhdK7bPqKPETqz050XoKMFBu9H6IKZnQWxT+yQKBgQCjzwXNT3C2y1ZaspplyAElCrb8rbljXt8mTRIh6CHdCMTAYbLBDOi6VpkmyriprfEdka2dV4eo4gjHjp6w4REB7L62VIg8Pe8rgToSOoJ0bP5litvm+XPZZYSDUMiBMp8CcADKp7Bx9PCqVlkqzG2WD+7iox5fk8tt0tQKO1ZeawKBgBSELR0GQPzfcfZFaztEfCa1HAy58MCdhjSHVrHzb2RRGxtp6aikftxM1d9WBdkEfze9u8bFWk4Rr89Pw/gg0brri3eiIjp+EZq3egVP4D85KiUH4doF9JUWXBFACG+UYfMVEESrriaSX7/1keIVm7EY87+BspJ493Uf8wsZcILhAoGAb7gs89a/P9w0XA09ojcjGLlTDcgKDUzSEpQzzq48Ejz9u1umh2WfgB65uyw9f3QjYxBNAWeGHVdzt258qpQFfnORqlbCx5tR+DDdgzsmZ/NCGcz3p3LwYQ+Mrea1qO/2ZLowwAZYH5B3ZyUXEUp1Xwgk26t2dqcc8hz9ArFveVsCgYB8OUqclLZZSbyuR9gyeHUWD2ZeZ2ExUaSSBCSBGtE0RIKW4iurtdNB23QZWVHppyL8xcyUD/a5BISnSesBUz7hC73XR3n8osDdWK6sbmDmKPqybtuVmioju6f73WlrM4WiE5gaUvM2C2tyf+jcHl+DBpANHXUkCsJaz5j+shtCWg==";
    //支付宝 appid
    public static String ALI_APPID="2021002134679748";


    //支付宝网关
    public static String ALI_GATEWAY_DOMAIN = "https://openapi.alipay.com/gateway.do";

    // 支付宝 支付回调  TODO:等到服务器上后这里需要改
    public static String ALI_NOTIFY_URL ="https://pay.quwentxw.com/pay/aliPayNotify";


}