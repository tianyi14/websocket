package com.ps.websocket.constant;

public class RedisConstants {

    public static String RABBIT_MQ_UNIQUE_RETRY_KEY = "RABBIT_MQ_UNIQUE_RETRY_KEY_%s";

    public static String PAY_LOCK_KEY_PRE="pay_lock_";

}
