package com.ps.websocket;


import com.ps.websocket.constant.PayConstants;
import com.ps.websocket.util.EnvironmentUtils;
import com.ps.websocket.util.PayUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;


@Slf4j
@Component
@DependsOn("springContextHolder")
public class ApplicationRunnerImpl implements ApplicationRunner {


    @Override
    public void run(ApplicationArguments args) throws Exception {
        // 读取微信支付证书信息 商户证书私钥
        try {
         /*   X509Certificate certificate = PayUtils.getCertificate(new FileInputStream(EnvironmentUtils.getString("wxpay.cert.location")));
            PrivateKey privateKey = PayUtils.getPrivateKey(EnvironmentUtils.getString("wxpay.cert.key.location"));
            String serialNo = certificate.getSerialNumber().toString(16).toUpperCase();
            PayConstants.serialNo = serialNo;
            PayConstants.certKey = privateKey;*/
            log.info("微信支付证书信息初始化成功");
        }catch (Exception e) {
            log.error("微信支付证书信息初始化失败，原因："+e.toString());
        }

    }





}
