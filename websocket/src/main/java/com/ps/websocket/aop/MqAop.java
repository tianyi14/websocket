package com.ps.websocket.aop;

import com.ps.websocket.common.CommonConsumerMqReq;
import com.ps.websocket.constant.RedisConstants;
import com.ps.websocket.util.JedisUtils;
import com.ps.websocket.util.ObjectUtils;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.amqp.core.Message;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Aspect
@Component
@Slf4j
public class MqAop {

    //对监听注解织入
    @Pointcut("@annotation(org.springframework.amqp.rabbit.annotation.RabbitHandler)")
    public void pointcut() {
    }

    @Around("pointcut()")
    public void pross(ProceedingJoinPoint pjp) throws Throwable {

        Signature signature = pjp.getSignature();
        String name = signature.getName();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();

        Object[] args = pjp.getArgs();
        //拿到 这两个参数 ack需要
        Message message = null;
        Channel channel = null;
        for (int i = 0; i < args.length; i++) {
            if (args[i] instanceof Message ) {
                message = (Message) args[i];
            }
            if (args[i] instanceof Channel ) {
                channel = (Channel) args[i];
            }
        }
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        CommonConsumerMqReq req = (CommonConsumerMqReq) ObjectUtils.unserialize(message.getBody());
        //是否成功消费
        boolean flag = true;
        String retryKey = String.format(RedisConstants.RABBIT_MQ_UNIQUE_RETRY_KEY, req.getUuid());

        try{
            pjp.proceed();
        }catch (Exception e) {
            flag = false;
            //如果发生异常 就重试  这里设置个重试次数 5次  当次数超过几次时候 消息就不重试，并把错误消息放至数据库中存储或者给当前队列预先绑定死信  后续人工处理
            if (!JedisUtils.exists(retryKey) || Long.valueOf(JedisUtils.get(retryKey)) < 5L) {
                log.info("消息重试投放");
                JedisUtils.incr(retryKey);
                // 是否批量  个人理解 没必要批量，反而可能造成消息 确认出错，如果deliveryTag 之前的消息还没执行完
                channel.basicNack(deliveryTag,false,true);
            } else {
                // 过了重试次数 就把消息丢弃  后续这里可以存库（或者不管，定义死信）
                log.info("消息重试丢弃");
                channel.basicNack(deliveryTag,false,false);
                //删除redis重试次数
                JedisUtils.del(retryKey);

            }
        }
        if (flag) {
            log.info("消息确认");
            channel.basicAck(deliveryTag,true);
            //删除redis重试次数
            if (JedisUtils.exists(retryKey)) {
                JedisUtils.del(retryKey);
            }
        }

    }

}
