package com.ps.websocket.util;

import com.alibaba.fastjson.JSONObject;
import com.ps.websocket.common.SpringContextHolder;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Jedis Cache 工具类
 */
@Slf4j
public class JedisUtils {

	public static final int DEFAULT_TIME = 60 * 10;// 10分钟

	@Autowired
	private static JedisPool jedisPool = SpringContextHolder.getBean(JedisPool.class);



	/** jedis阻塞锁，60s并发阻塞时间 */
	public static void blockLock(String key) {
		try {
			while (!lock(key)) {

				Thread.sleep(100);
			}
		} catch (Exception e) {
			unlock(key);
		}
	}

	/** jedis阻塞锁，times为毫秒并发阻塞时间 */
	public static boolean blockLock(String key, Integer times) {
		try {
			while (!lock(key)) {
				Thread.sleep(times);
			}
		} catch (Exception e) {
			unlock(key);
		}
		return true;
	}

	/**
	 * 获取缓存
	 * 
	 * @param key 键
	 * @return 值
	 */
	public static String get(String key) {
		Jedis jedis =null;
		String value = null;
		try {
			jedis = jedisPool.getResource();
			if (jedis.exists(key)) {
				value = jedis.get(key);
				value = StringUtils.isNotBlank(value) && !"nil".equalsIgnoreCase(value) ? value : null;
			}
		} catch (Exception e) {

		}
		return value;
	}

	/**
	 * 获取缓存
	 * 
	 * @param key 键
	 * @return 值
	 */
	public static <T> T getUnSerializeObject(String key, Class<T> classOfT) {
		String value = null;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			if (jedis.exists(key)) {
				value = jedis.get(key);
				return JSONObject.parseObject(value, classOfT);
			}
		} catch (Exception e) {
		}
		return null;
	}

	/**
	 * 获取缓存 Type typeOfT 反射
	 * 
	 * @param key 键
	 * @return 值
	 */
	public static <T> T getUnSerializeObject(String key, Type typeOfT) {
		String value = null;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			if (jedis.exists(key)) {
				value = jedis.get(key);
				return JSONObject.parseObject(value, typeOfT);
			}
		} catch (Exception e) {
			log.warn("getObject {} = {}", key, value, e);
		}
		return null;
	}

	/**
	 * 获取缓存
	 * 
	 * @param key 键
	 * @return 值
	 */
	public static Object getObject(String key) {
		Object value = null;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			if (jedis.exists(getBytesKey(key))) {
				value = toObject(jedis.get(getBytesKey(key)));
				log.debug("getObject {} = {}", key, value);
			}
		} catch (Exception e) {
			log.warn("getObject {} = {}", key, value, e);
		}
		return value;
	}

	/**
	 * 发布消息
	 * 
	 * @param channel 渠道名
	 * @param message 消息内容
	 * @return
	 */
	public static Long publish(String channel, String message) {
		Long result = null;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.publish(channel, message);
		} catch (Exception e) {
			log.warn("set {} = {}", channel, message, e);
		}
		return result;
	}

	/**
	 * 设置缓存
	 * 
	 * @param key 键
	 * @return 值
	 */
	public static Object setUnSerializeObject(String key, Object value, int cacheSeconds) {
		String result = null;
		if (value != null) {
			Jedis jedis =null;
			try {
				jedis = jedisPool.getResource();
				result = jedis.set(key, JSONObject.toJSONString(value));
				if (cacheSeconds != 0) {
					jedis.expire(key, cacheSeconds);
				}
				log.debug("setObject {} = {}", key, value);
			} catch (Exception e) {
				log.warn("setObject {} = {}", key, value, e);
			}
		}
		return result;
	}

	/**
	 * 设置缓存
	 * 
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public static String set(String key, String value) {
		String result = null;
		if (StringUtils.isNotBlank(value)) {
			Jedis jedis =null;
			try {
				jedis = jedisPool.getResource();
				result = jedis.set(key, value);
				jedis.expire(key, DEFAULT_TIME);
				log.debug("set {} = {}", key, value);
			} catch (Exception e) {
				log.warn("set {} = {}", key, value, e);
			}
		}
		return result;
	}

	/**
	 * 设置缓存
	 * 
	 * @param key 键
	 * @param value 值
	 * @param cacheSeconds 超时时间，0为不超时
	 * @return
	 */
	public static String set(String key, String value, int cacheSeconds) {
		String result = null;
		if (StringUtils.isNotBlank(value)) {
			Jedis jedis =null;
			try {
				jedis = jedisPool.getResource();
				if (value != null) {
					result = jedis.set(key, value);
					if (cacheSeconds != 0) {
						jedis.expire(key, cacheSeconds);
					}
					log.debug("set {} = {}", key, value);
				}
			} catch (Exception e) {
				log.warn("set {} = {}", key, value, e);
			}
		}
		return result;
	}

	/***
	 * 原子get,set操作
	 * 
	 * @param key
	 * @param value
	 * @param cacheSeconds
	 * @return
	 */
	public static String getSet(String key, String value, int cacheSeconds) {
		String result = null;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.getSet(key, value);
			if (cacheSeconds != 0) {
				jedis.expire(key, cacheSeconds);
			}
			log.debug("set {} = {}", key, value);
		} catch (Exception e) {
			log.warn("set {} = {}", key, value, e);
		}
		return result;
	}

	/**
	 * 设置缓存
	 * 
	 * @param key 键
	 * @param value 值
	 * @param cacheSeconds 超时时间，0为不超时
	 * @return
	 */
	public static String setObject(String key, Object value, int cacheSeconds) {
		String result = null;
		if (value != null) {
			Jedis jedis =null;
			try {
				jedis = jedisPool.getResource();
				result = jedis.set(getBytesKey(key), toBytes(value));
				if (cacheSeconds != 0) {
					jedis.expire(key, cacheSeconds);
				}
				log.debug("setObject {} = {}", key, value);
			} catch (Exception e) {
				log.warn("setObject {} = {}", key, value, e);
			}
		}
		return result;
	}

	/**
	 * 设置缓存
	 * 
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public static String setObject(String key, Object value) {
		String result = null;
		if (value != null) {
			Jedis jedis =null;
			try {
				jedis = jedisPool.getResource();
				result = jedis.set(getBytesKey(key), toBytes(value));
				log.debug("setObject {} = {}", key, value);
			} catch (Exception e) {
				log.warn("setObject {} = {}", key, value, e);
			}
		}
		return result;
	}

	/**
	 * 获取List缓存
	 * 
	 * @param key 键
	 * @return 值
	 */
	public static List<String> getList(String key) {
		List<String> value = null;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			if (jedis.exists(key)) {
				value = jedis.lrange(key, 0, -1);
				log.debug("getList {} = {}", key, value);
			}
		} catch (Exception e) {
			log.warn("getList {} = {}", key, value, e);
		}
		return value;
	}

	/**
	 * 获取List缓存
	 * 
	 * @param key 键
	 * @return 值
	 */
	public static List<Object> getObjectList(String key) {
		List<Object> value = null;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			if (jedis.exists(getBytesKey(key))) {
				List<byte[]> list = jedis.lrange(getBytesKey(key), 0, -1);
				value = Lists.newArrayList();
				for (byte[] bs : list) {
					value.add(toObject(bs));
				}
				log.debug("getObjectList {} = {}", key, value);
			}
		} catch (Exception e) {
			log.warn("getObjectList {} = {}", key, value, e);
		}
		return value;
	}

	/**
	 * 设置List缓存
	 * 
	 * @param key 键
	 * @param value 值
	 * @param cacheSeconds 超时时间，0为不超时
	 * @return
	 */
	public static long setList(String key, List<String> value, int cacheSeconds) {
		long result = 0;
		if (value != null) {
			Jedis jedis =null;
			try {
				jedis = jedisPool.getResource();
				if (jedis.exists(key)) {
					jedis.del(key);
				}
				result = jedis.rpush(key, value.toArray(new String[value.size()]));
				if (cacheSeconds != 0) {
					jedis.expire(key, cacheSeconds);
				}
				log.debug("setList {} = {}", key, value);
			} catch (Exception e) {
				log.warn("setList {} = {}", key, value, e);
			}
		}
		return result;
	}

	/**
	 * 设置List缓存
	 * 
	 * @param key 键
	 * @param value 值
	 * @param cacheSeconds 超时时间，0为不超时
	 * @return
	 */
	public static long setObjectList(String key, List<Object> value, int cacheSeconds) {
		long result = 0;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			if (jedis.exists(getBytesKey(key))) {
				jedis.del(key);
			}
			List<byte[]> list = Lists.newArrayList();
			for (Object o : value) {
				list.add(toBytes(o));
			}
			result = jedis.rpush(getBytesKey(key), (byte[][]) list.toArray());
			if (cacheSeconds != 0) {
				jedis.expire(key, cacheSeconds);
			}
			log.debug("setObjectList {} = {}", key, value);
		} catch (Exception e) {
			log.warn("setObjectList {} = {}", key, value, e);
		}
		return result;
	}

	/**
	 * 向List缓存中添加值
	 * 
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public static long listAdd(String key, String... value) {
		long result = 0;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.rpush(key, value);
			log.debug("listAdd {} = {}", key, value);
		} catch (Exception e) {
			log.warn("listAdd {} = {}", key, value, e);
		}
		return result;
	}

	/**
	 * 向List缓存中添加值
	 * 
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public static long listObjectAdd(String key, Object... value) {
		long result = 0;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			List<byte[]> list = Lists.newArrayList();
			for (Object o : value) {
				list.add(toBytes(o));
			}
			result = jedis.rpush(getBytesKey(key), (byte[][]) list.toArray());
			log.debug("listObjectAdd {} = {}", key, value);
		} catch (Exception e) {
			log.warn("listObjectAdd {} = {}", key, value, e);
		}
		return result;
	}

	/**
	 * 获取缓存
	 * 
	 * @param key 键
	 * @return 值
	 */
	public static Set<String> getSet(String key) {
		Set<String> value = null;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			if (jedis.exists(key)) {
				value = jedis.smembers(key);
				log.debug("getSet {} = {}", key, value);
			}
		} catch (Exception e) {
			log.warn("getSet {} = {}", key, value, e);
		}
		return value;
	}

	/**
	 * 获取缓存
	 * 
	 * @param key 键
	 * @return 值
	 */
	public static Set<Object> getObjectSet(String key) {
		Set<Object> value = null;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			if (jedis.exists(getBytesKey(key))) {
				value = Sets.newHashSet();
				Set<byte[]> set = jedis.smembers(getBytesKey(key));
				for (byte[] bs : set) {
					value.add(toObject(bs));
				}
				log.debug("getObjectSet {} = {}", key, value);
			}
		} catch (Exception e) {
			log.warn("getObjectSet {} = {}", key, value, e);
		}
		return value;
	}

	/**
	 * 设置Set缓存
	 * 
	 * @param key 键
	 * @param value 值
	 * @param cacheSeconds 超时时间，0为不超时
	 * @return
	 */
	public static long setSet(String key, Set<String> value, int cacheSeconds) {
		long result = 0;
		if (value != null && value.size() > 0) {
			Jedis jedis =null;
			try {
				jedis = jedisPool.getResource();
				if (jedis.exists(key)) {
					jedis.del(key);
				}
				result = jedis.sadd(key, value.toArray(new String[value.size()]));
				if (cacheSeconds != 0) {
					jedis.expire(key, cacheSeconds);
				}
				log.debug("setSet {} = {}", key, value);
			} catch (Exception e) {
				log.warn("setSet {} = {}", key, value, e);
			}
		}

		return result;
	}

	/**
	 * 设置Set缓存
	 * 
	 * @param key 键
	 * @param value 值
	 * @param cacheSeconds 超时时间，0为不超时
	 * @return
	 */
	public static long setObjectSet(String key, Set<Object> value, int cacheSeconds) {
		long result = 0;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			if (jedis.exists(getBytesKey(key))) {
				jedis.del(key);
			}
			Set<byte[]> set = Sets.newHashSet();
			for (Object o : value) {
				set.add(toBytes(o));
			}
			result = jedis.sadd(getBytesKey(key), (byte[][]) set.toArray());
			if (cacheSeconds != 0) {
				jedis.expire(key, cacheSeconds);
			}
			log.debug("setObjectSet {} = {}", key, value);
		} catch (Exception e) {
			log.warn("setObjectSet {} = {}", key, value, e);
		}
		return result;
	}

	/**
	 * 向Set缓存中添加值
	 * 
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public static long setSetAdd(String key, String... value) {
		long result = 0;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.sadd(key, value);
			log.debug("setSetAdd {} = {}", key, value);
		} catch (Exception e) {
			log.warn("setSetAdd {} = {}", key, value, e);
		}
		return result;
	}

	/**
	 * 向Set缓存中添加值
	 * 
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public static long setSetObjectAdd(String key, Object... value) {
		long result = 0;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			Set<byte[]> set = Sets.newHashSet();
			for (Object o : value) {
				set.add(toBytes(o));
			}
			result = jedis.rpush(getBytesKey(key), (byte[][]) set.toArray());
			log.debug("setSetObjectAdd {} = {}", key, value);
		} catch (Exception e) {
			e.printStackTrace();
			log.warn("setSetObjectAdd {} = {}", key, value, e);
		}
		return result;
	}

	/**
	 * 获取Map缓存
	 * 
	 * @param key 键
	 * @return 值
	 */
	public static Map<String, String> getMap(String key) {
		Map<String, String> value = null;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			if (jedis.exists(key)) {
				value = jedis.hgetAll(key);
				log.debug("getMap {} = {}", key, value);
			}
		} catch (Exception e) {
			log.warn("getMap {} = {}", key, value, e);
		}
		return value;
	}

	/**
	 * 获取Map缓存
	 * 
	 * @param key 键
	 * @return 值
	 */
	public static Map<String, Object> getObjectMap(String key) {
		Map<String, Object> value = null;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			if (jedis.exists(getBytesKey(key))) {
				value = Maps.newHashMap();
				Map<byte[], byte[]> map = jedis.hgetAll(getBytesKey(key));
				for (Map.Entry<byte[], byte[]> e : map.entrySet()) {
					value.put(StringUtils.toString(e.getKey()), toObject(e.getValue()));
				}
				log.debug("getObjectMap {} = {}", key, value);
			}
		} catch (Exception e) {
			log.warn("getObjectMap {} = {}", key, value, e);
		}
		return value;
	}

	/**
	 * 设置Map缓存
	 * 
	 * @param key 键
	 * @param value 值
	 * @param cacheSeconds 超时时间，0为不超时
	 * @return
	 */
	public static String setMap(String key, Map<String, String> value, int cacheSeconds) {
		String result = null;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			if (jedis.exists(key)) {
				jedis.del(key);
			}
			result = jedis.hmset(key, value);
			if (cacheSeconds != 0) {
				jedis.expire(key, cacheSeconds);
			}
			log.debug("setMap {} = {}", key, value);
		} catch (Exception e) {
			log.warn("setMap {} = {}", key, value, e);
		}
		return result;
	}

	/**
	 * 设置Map缓存
	 * 
	 * @param key 键
	 * @param value 值
	 * @param cacheSeconds 超时时间，0为不超时
	 * @return
	 */
	public static String setObjectMap(String key, Map<String, Object> value, int cacheSeconds) {
		String result = null;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			if (jedis.exists(getBytesKey(key))) {
				jedis.del(key);
			}
			Map<byte[], byte[]> map = Maps.newHashMap();
			for (Map.Entry<String, Object> e : value.entrySet()) {
				map.put(getBytesKey(e.getKey()), toBytes(e.getValue()));
			}
			result = jedis.hmset(getBytesKey(key), (Map<byte[], byte[]>) map);
			if (cacheSeconds != 0) {
				jedis.expire(key, cacheSeconds);
			}
			log.debug("setObjectMap {} = {}", key, value);
		} catch (Exception e) {
			log.warn("setObjectMap {} = {}", key, value, e);
		}
		return result;
	}

	public static Long incr(String key) {
		Long result = null;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.incr(key);
		} catch (Exception e) {
			log.warn("incr {}", key, e);
		}
		return result;
	}

	public static Long decr(String key) {
		Long result = null;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.decr(key);
		} catch (Exception e) {
			log.warn("decr {}", key, e);
		}
		return result;
	}

	public static Long decrBy(String key, long decrement) {
		Long result = null;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.decrBy(key, decrement);
		} catch (Exception e) {
			log.warn("decr {}", key, e);
		}
		return result;
	}

	public static Long incrBy(String key, long decrement) {
		Long result = null;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.incrBy(key, decrement);
		} catch (Exception e) {
			log.warn("decr {}", key, e);
		}
		return result;
	}

	/**
	 * 向Map缓存中添加值
	 * 
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public static String mapPut(String key, Map<String, String> value) {
		String result = null;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.hmset(key, value);
			log.debug("mapPut {} = {}", key, value);
		} catch (Exception e) {
			log.warn("mapPut {} = {}", key, value, e);
		}
		return result;
	}

	/**
	 * 向Map缓存中添加值
	 * 
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public static String mapObjectPut(String key, Map<String, Object> value) {
		String result = null;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			Map<byte[], byte[]> map = Maps.newHashMap();
			for (Map.Entry<String, Object> e : value.entrySet()) {
				map.put(getBytesKey(e.getKey()), toBytes(e.getValue()));
			}
			result = jedis.hmset(getBytesKey(key), (Map<byte[], byte[]>) map);
			log.debug("mapObjectPut {} = {}", key, value);
		} catch (Exception e) {
			log.warn("mapObjectPut {} = {}", key, value, e);
		}
		return result;
	}

	/**
	 * 移除Map缓存中的值
	 * 
	 * @param key 键
	 * @param mapKey 值
	 * @return
	 */
	public static long mapRemove(String key, String mapKey) {
		long result = 0;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.hdel(key, mapKey);
			log.debug("mapRemove {}  {}", key, mapKey);
		} catch (Exception e) {
			log.warn("mapRemove {}  {}", key, mapKey, e);
		}
		return result;
	}

	/**
	 * 移除Map缓存中的值
	 * 
	 * @param key 键
	 * @param mapKey 值
	 * @return
	 */
	public static long mapObjectRemove(String key, String mapKey) {
		long result = 0;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.hdel(getBytesKey(key), getBytesKey(mapKey));
			log.debug("mapObjectRemove {}  {}", key, mapKey);
		} catch (Exception e) {
			log.warn("mapObjectRemove {}  {}", key, mapKey, e);
		}
		return result;
	}

	/**
	 * 判断Map缓存中的Key是否存在
	 * 
	 * @param key 键
	 * @param mapKey 值
	 * @return
	 */
	public static boolean mapExists(String key, String mapKey) {
		boolean result = false;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.hexists(key, mapKey);
			log.debug("mapExists {}  {}", key, mapKey);
		} catch (Exception e) {
			log.warn("mapExists {}  {}", key, mapKey, e);
		}
		return result;
	}

	/**
	 * 判断Map缓存中的Key是否存在
	 * 
	 * @param key 键
	 * @param mapKey 值
	 * @return
	 */
	public static boolean mapObjectExists(String key, String mapKey) {
		boolean result = false;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.hexists(getBytesKey(key), getBytesKey(mapKey));
			log.debug("mapObjectExists {}  {}", key, mapKey);
		} catch (Exception e) {
			log.warn("mapObjectExists {}  {}", key, mapKey, e);
		}
		return result;
	}

	/**
	 * 删除缓存
	 * 
	 * @param key 键
	 * @return
	 */
	public static long del(String key) {
		long result = 0;
		if (StringUtils.isNotBlank(key)) {
			Jedis jedis =null;
			try {
				jedis = jedisPool.getResource();
				if (jedis.exists(key)) {
					result = jedis.del(key);
					log.debug("del {}", key);
				} else {
					log.debug("del {} not exists", key);
				}
			} catch (Exception e) {
				log.warn("del {}", key, e);
			}
		}
		return result;
	}

	/**
	 * 删除缓存
	 * 
	 * @param key 键
	 * @return
	 */
	public static long delObject(String key) {
		long result = 0;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			if (jedis.exists(getBytesKey(key))) {
				result = jedis.del(getBytesKey(key));
				log.debug("delObject {}", key);
			} else {
				log.debug("delObject {} not exists", key);
			}
		} catch (Exception e) {
			log.warn("delObject {}", key, e);
		}
		return result;
	}

	/**
	 * 缓存是否存在
	 * 
	 * @param key 键
	 * @return
	 */
	public static boolean exists(String key) {
		boolean result = false;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			if (jedis != null) {
				result = jedis.exists(key);
			}
			log.debug("exists {}", key);
		} catch (Exception e) {
			log.warn("exists {}", key, e);
		}
		return result;
	}

	/**
	 * 缓存是否存在
	 * 
	 * @param key 键
	 * @return
	 */
	public static boolean existsObject(String key) {
		boolean result = false;
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			result = jedis.exists(getBytesKey(key));
			log.debug("existsObject {}", key);
		} catch (Exception e) {
			log.warn("existsObject {}", key, e);
		}
		return result;
	}

	/** 获取包含前缀的Key */
	/*public static TreeSet<String> keys(String pattern) {
		TreeSet<String> keys = new TreeSet<String>();
		Map<String, JedisPool> clusterNodes = jedisCluster.getClusterNodes();
		for (String k : clusterNodes.keySet()) {
			JedisPool jp = clusterNodes.get(k);
			Jedis connection = jp.getResource();
			try {
				keys.addAll(connection.keys(pattern));
			} catch (Exception e) {
				log.error("Getting keys error: {}", e);
			} finally {
				log.debug("Connection closed.");
				connection.close();// 用完一定要close这个链接！！！
			}
		}
		return keys;
	}*/

	/**
	 * 获取锁 ,在指定时间内内重复调用返回false
	 * 
	 * @param key 锁key
	 * @param time 秒
	 * @return
	 */
	public static boolean lock(String key, long time) {
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			String result =  jedis.set(key, "S", "NX", "PX", time * 1000);
			return "OK".equals(result);
		} catch (Exception e) {
			log.warn("lock.", e);
			e.printStackTrace();
			return false;
		}
	}

	/***
	 * 获取锁
	 */
	public static boolean lock(String key, String value, int cacheSeconds) {
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			Long result = jedis.setnx(key, value);
			if (cacheSeconds != 0) {
				jedis.expire(key, cacheSeconds);
			}
			return result > 0;
		} catch (Exception e) {
			log.warn("lock.", e);
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 获取锁
	 * 
	 * @param key 键
	 * @param value 值
	 * @param time 单位：秒
	 * @return
	 */
	public static boolean lockByNX(String key, String value, long time) {
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			String result =   jedis.set(key, value, "NX", "PX", time * 1000);
			return "OK".equals(result);
		} catch (Exception e) {
			log.warn("lock.", e);
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 指定重试次数获取锁
	 * 
	 * @param key 键
	 * @param time 过期时间，单位：秒
	 * @param retryCount 重试次数
	 * @return
	 */
	public static boolean lockByRetryCount(String key, long time, int retryCount) {
		try {
			int count = 0;
			while ((retryCount > count)) {
				boolean isSuccess = lock(key, time);
				if (isSuccess) {
					return true;
				}
				count++;
				Thread.sleep(100);
			}
		} catch (Exception e) {
			log.warn("lock.", e);
			e.printStackTrace();
			return false;
		}
		return false;
	}

	/** 列表头部插入 */
	public static Boolean lpush(String key, String value) {
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			Long result = jedis.lpush(key, value);
			if (result > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			log.info("key:" + key + " value:" + value + " lpush异常：" + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	/** 列表尾部插入 */
	public static Boolean rpush(String key, String value) {
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			Long result = jedis.rpush(key, value);
			if (result > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			log.info("key:" + key + " value:" + value + " rpush异常：" + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	public static String lpop(String key) {
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			return jedis.lpop(key);
		} catch (Exception e) {
			log.info("key:" + key + " lpop异常：" + e.getMessage());
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * 获取key的剩余生存时间
	 * 
	 * @param xKey
	 * @return
	 */
	public static long ttl(String xKey) {
		Jedis jedis =null;
		try {
			jedis = jedisPool.getResource();
			Long result = jedis.ttl(xKey);
			return result;
		} catch (Exception ex) {
			log.error("ttl方法取得" + xKey + "的过期时间时出现系统错误", ex);
			ex.printStackTrace();
			return -1;
		}
	}

	/***
	 * 获取锁60s，60s内重复调用返回false
	 * 
	 * @param key 锁key
	 * @return
	 */
	public static boolean lock(String key) {
		return lock(key, 60);
	}

	public static void unlock(String key) {
		del(key);
	}

	/**
	 * 获取byte[]类型Key
	 * 
	 * @param object
	 * @return
	 */
	public static byte[] getBytesKey(Object object) {
		if (object instanceof String) {
			return StringUtils.getBytes((String) object);
		} else {
			return ObjectUtils.serialize(object);
		}
	}

	/**
	 * Object转换byte[]类型
	 * 
	 * @param object
	 * @return
	 */
	public static byte[] toBytes(Object object) {
		return ObjectUtils.serialize(object);
	}

	/**
	 * byte[]型转换Object
	 * 
	 * @param bytes
	 * @return
	 */
	public static Object toObject(byte[] bytes) {
		return ObjectUtils.unserialize(bytes);
	}

}
