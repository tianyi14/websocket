package com.ps.websocket.util;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ps.websocket.constant.PayConstants;
import com.ps.websocket.domain.CertificateVo;
import org.springframework.util.Base64Utils;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.*;
import java.security.cert.Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

//支付工具
public class PayUtils {


    /**
     * 锁对象，可以为任意对象
     */
    private static Object lockObj = "lockerOrder";

    /**
     * 订单号生成计数器
     */
    private static long orderNumCount = 0L;

    /**
     * 每毫秒生成订单号数量最大值
     */
    private static int maxPerMSECSize = 1000;


    // 定义全局容器 保存微信平台证书公钥  注意线程安全
    public static final Map<String, Certificate> CERTIFICATE_MAP = new ConcurrentHashMap<>();

    public static void refreshCertificate() throws CertificateException, ParseException, UnsupportedEncodingException, SignatureException, NoSuchAlgorithmException, InvalidKeyException {

        //获取平台证书json
        Map<String,String>  headerMap = new HashMap<>();
        String schema = "WECHATPAY2-SHA256-RSA2048 ";

        headerMap.put("Authorization",schema+ PayUtils.getToken("GET", PayConstants.WX_CERTIFICATES_URL, ""));
        headerMap.put("Content-Type", "application/json");
        headerMap.put("Accept", "application/json");
        String result = HttpUtils.get(PayConstants.WX_PAY_DOMAIN_URL+PayConstants.WX_CERTIFICATES_URL, String.class,headerMap,null);

        JSONObject jsonObject = JSONObject.parseObject(result);
        List<CertificateVo> certificateList = JSON.parseArray(jsonObject.getString("data"),CertificateVo.class);
        //最新证书响应实体类
        CertificateVo newestCertificate = null;
        //最新时间
        Date newestTime = null;
        for (CertificateVo certificate:certificateList){
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            //如果最新时间是null
            if (newestTime == null){
                newestCertificate = certificate;
                //设置最新启用时间
                newestTime = formatter.parse(certificate.getEffective_time());
            }else{
                Date effectiveTime = formatter.parse(certificate.getEffective_time());
                //如果启用时间大于最新时间
                if (effectiveTime.getTime() > newestTime.getTime()){
                    //更换最新证书响应实体类
                    newestCertificate = certificate;
                }
            }
        }

        CertificateVo.EncryptCertificate encryptCertificate = newestCertificate.getEncrypt_certificate();



        String associatedData = encryptCertificate.getAssociated_data();
        String nonce = encryptCertificate.getNonce();
        String ciphertext = encryptCertificate.getCiphertext();
        //调用微信接口 获取平台文档
        String publicKey = decryptResponseBody(associatedData, nonce, ciphertext);

        final CertificateFactory cf = CertificateFactory.getInstance("X509");

        ByteArrayInputStream inputStream = new ByteArrayInputStream(publicKey.getBytes(StandardCharsets.UTF_8));
        Certificate certificate = null;
        try {

            certificate =  cf.generateCertificate(inputStream);
        } catch (CertificateException e) {
            e.printStackTrace();
        }
        String responseSerialNo =newestCertificate.getSerial_no();
        // 清理HashMap
        CERTIFICATE_MAP.clear();
        // 放入证书
        CERTIFICATE_MAP.put(responseSerialNo, certificate);
    }




    /**
     * 解密响应体.
     *
     * @param associatedData  response.body.data[i].encrypt_certificate.associated_data
     * @param nonce          response.body.data[i].encrypt_certificate.nonce
     * @param ciphertext     response.body.data[i].encrypt_certificate.ciphertext
     * @return the string
     * @throws GeneralSecurityException the general security exception
     */
    public static String decryptResponseBody(String associatedData, String nonce, String ciphertext) {
        try {
            Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");

            SecretKeySpec key = new SecretKeySpec(PayConstants.wxSecretKey.getBytes(StandardCharsets.UTF_8), "AES");
            GCMParameterSpec spec = new GCMParameterSpec(128, nonce.getBytes(StandardCharsets.UTF_8));

            cipher.init(Cipher.DECRYPT_MODE, key, spec);
            cipher.updateAAD(associatedData.getBytes(StandardCharsets.UTF_8));

            byte[] bytes;
            try {
                bytes = cipher.doFinal(Base64Utils.decodeFromString(ciphertext));
            } catch (GeneralSecurityException e) {
                throw new IllegalArgumentException(e);
            }
            return new String(bytes, StandardCharsets.UTF_8);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new IllegalStateException(e);
        } catch (InvalidKeyException | InvalidAlgorithmParameterException e) {
            throw new IllegalArgumentException(e);
        }
    }


    public static String getToken(String method, String canonicalUrl, String body) throws UnsupportedEncodingException, SignatureException, NoSuchAlgorithmException, InvalidKeyException {
        //随机串
        String nonceStr = getRandomString(32);
        long timestamp = System.currentTimeMillis() / 1000;
        String message = buildMessage(method, canonicalUrl, timestamp, nonceStr, body);
        String signature = sign(message.getBytes("utf-8"));

        return "mchid=\"" + PayConstants.wxMchid + "\","
                + "nonce_str=\"" + nonceStr + "\","
                + "timestamp=\"" + timestamp + "\","
                + "serial_no=\"" + PayConstants.serialNo + "\","
                + "signature=\"" + signature + "\"";
    }

    public static String getSecondSign(String prepayId,String nonceStr,long time) throws UnsupportedEncodingException, SignatureException, NoSuchAlgorithmException, InvalidKeyException {

        String message = buildSecondMessage(prepayId, time, nonceStr);
        String signature = sign(message.getBytes("utf-8"));

        return signature;
    }


    public static String buildSecondMessage(String prepayId,long timestamp, String nonceStr) {

        return PayConstants.wxAppid + "\n"
                + timestamp + "\n"
                + nonceStr + "\n"
                + prepayId + "\n";
    }

    public static String sign(byte[] message) throws SignatureException, NoSuchAlgorithmException, InvalidKeyException {
        Signature sign = Signature.getInstance("SHA256withRSA");
        sign.initSign(PayConstants.certKey);
        sign.update(message);

        return Base64.getEncoder().encodeToString(sign.sign());
    }

    /*public static PrivateKey privateKeyGet(){
        try {
            String privateKey = PayConstants.mchSecret.replace("-----BEGIN PRIVATE KEY-----", "")
                    .replace("-----END PRIVATE KEY-----", "")
                    .replaceAll("\\s+", "");

            KeyFactory kf = KeyFactory.getInstance("RSA");
            return kf.generatePrivate(
                    new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKey)));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("当前Java环境不支持RSA", e);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException("无效的密钥格式");
        }
    }
*/


    public static String buildMessage(String method, String url, long timestamp, String nonceStr, String body) {
        String canonicalUrl =url;

        return method + "\n"
                + canonicalUrl + "\n"
                + timestamp + "\n"
                + nonceStr + "\n"
                + body + "\n";
    }

    // 随机字符串生成
    public static String getRandomString(int length) { // length表示生成字符串的长度
        String base = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        Random random = new Random();
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }

    /**
     * 生成非重复订单号，理论上限1毫秒1000个，可扩展
     */
    public static String makeOrderNum() {
        try {
            // 最终生成的订单号
            String finOrderNum = "";
            synchronized (lockObj) {
                // 取系统当前时间作为订单号变量前半部分，精确到毫秒
                long nowLong = Long.parseLong(
                        new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
                // 计数器到最大值归零，可扩展更大，目前1毫秒处理峰值1000个，1秒100万
                if (orderNumCount >= maxPerMSECSize) {
                    orderNumCount = 0L;
                }
                // 组装订单号
                String countStr = maxPerMSECSize + orderNumCount + "";
                finOrderNum =  nowLong + countStr.substring(1);
                orderNumCount++;
            }
            return finOrderNum;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }


    /**
     * 获取证书。
     *
     * @param fis 证书文件流
     * @return X509证书
     */
    public static X509Certificate getCertificate(InputStream fis) throws IOException {
        BufferedInputStream bis = new BufferedInputStream(fis);
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X509");
            X509Certificate cert = (X509Certificate) cf.generateCertificate(bis);
            cert.checkValidity();
            return cert;
        } catch (CertificateExpiredException e) {
            throw new RuntimeException("证书已过期", e);
        } catch (CertificateNotYetValidException e) {
            throw new RuntimeException("证书尚未生效", e);
        } catch (CertificateException e) {
            throw new RuntimeException("无效的证书文件", e);
        } finally {
            bis.close();
        }
    }

    /**
     * 获取私钥。
     *
     * @param filename 私钥文件路径  (required)
     * @return 私钥对象
     */
    public static PrivateKey getPrivateKey(String filename) throws IOException {

        String content = new String(Files.readAllBytes(Paths.get(filename)), "utf-8");
        try {
            String privateKey = content.replace("-----BEGIN PRIVATE KEY-----", "")
                    .replace("-----END PRIVATE KEY-----", "")
                    .replaceAll("\\s+", "");

            KeyFactory kf = KeyFactory.getInstance("RSA");
            return kf.generatePrivate(
                    new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKey)));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("当前Java环境不支持RSA", e);
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException("无效的密钥格式");
        }
    }

}
