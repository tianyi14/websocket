package com.ps.websocket.util;

import com.ps.websocket.common.SpringContextHolder;
import org.springframework.core.env.Environment;

/**
 * @Description
 * @Date 2021/09/03
 * @Created by PeiShuai
 */
public class EnvironmentUtils {



    public static boolean isDev() {
        String env = getString("spring.profiles.active");
        return StringUtils.equals(env, "dev");
    }

    public static boolean isProd() {
        String env = getString("spring.profiles.active");
        return StringUtils.equals(env, "prod");
    }

    /**
     * 获取属性
     *
     * @param key
     * @return
     */
    public static String getString(String key) {
        Environment env = SpringContextHolder.getBean(Environment.class);
        return env.getProperty(key);
    }

}
