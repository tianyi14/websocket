/**
 *
 */
package com.ps.websocket.util;


import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.*;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.util.MimeTypeUtils;

import javax.net.ssl.SSLContext;
import java.io.*;
import java.lang.reflect.Field;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
public class HttpUtils {


    public static final String DEFAULT_ENCODING = "utf8";
    public static final String JSON_CONTENT_TYPE = "application/json;chatset=utf8";
    public static final String XML_CONTENT_TYPE = "text/xml";

    private static HttpClient client;

    public void setHttpClient(HttpClient client) {
        this.client = client;
    }

    public static void download(String url, File file, Header... header) {
        InputStream in = null;
        FileOutputStream fout = null;
        long startTime = System.currentTimeMillis();
        long endTime = 0L;
        HttpClient client = createClient(url);
        try {
            HttpGet get = new HttpGet(url);
            if (header != null) {
                for (Header each : header) {
                    get.setHeader(each);
                }
            }
            HttpResponse response = client.execute(get);
            HttpEntity entity = response.getEntity();
            in = entity.getContent();
            fout = new FileOutputStream(file);
            int l = -1;
            byte[] tmp = new byte[4096];
            while ((l = in.read(tmp)) != -1) {
                fout.write(tmp, 0, l);
            }
            fout.flush();
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        } finally {
            endTime = System.currentTimeMillis();
            System.out.println("httpUtils.downloadUrl:" + url + ",time:" + (endTime - startTime));
            // 关闭低层流。
            try {
                fout.close();
            }catch (Exception e) {

            }
            try {
                in.close();
            }catch (Exception e) {

            }/*
            CloseUtils.close(fout);
            CloseUtils.close(in);*/
        }
    }

    public static InputStream getUrlInputStream(String url, Header... header) {
        InputStream in = null;
        FileOutputStream fout = null;
        HttpClient client = createClient(url);
        try {
            HttpGet get = new HttpGet(url);
            if (header != null) {
                for (Header each : header) {
                    get.setHeader(each);
                }
            }
            HttpResponse response = client.execute(get);


            if (response.getEntity().getContentType().getValue().equals(ContentType.create(MimeTypeUtils.IMAGE_JPEG_VALUE).getMimeType())) {
                HttpEntity entity = response.getEntity();
                in = entity.getContent();

                return in;
            } else {
                return in;

            }
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public static InputStream getUrlInputStream(String url, String jsonString, Header... headers) {
        InputStream in = null;
        HttpClient client = createClient(url);
        HttpPost httpPost = null;
        try {
            if (headers != null) {
                for (Header each : headers) {
                    httpPost.setHeader(each);
                }
            }
            httpPost= new HttpPost(url);

            if (StringUtils.isNotEmpty(jsonString)) {
                StringEntity s = new StringEntity(jsonString, DEFAULT_ENCODING);
                s.setContentType(JSON_CONTENT_TYPE);
                httpPost.setEntity(s);
            }
            HttpResponse response = client.execute(httpPost);

            if (response.getEntity().getContentType().getValue().equals(ContentType.create(MimeTypeUtils.IMAGE_JPEG_VALUE).getMimeType())) {
                HttpEntity entity = response.getEntity();
                in = entity.getContent();
                //copy 一份输入流  不然释放连接时候资源也关闭
                return copyInputStream(in);
            } else {
                return in;
            }

        } catch (Exception e) {
            throw new IllegalStateException(e);
        } finally {
            //归还请求到连接池，注意 这里归还后 流也关闭了，所以上面copy一份输入流
            if (null != httpPost) {
                System.out.println("不归还连接池");
            }
        }

    }

    public static InputStream copyInputStream(InputStream in){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        byte[] buffer = new byte[1024];
        int len;
        try {
            while ((len = in.read(buffer)) > -1 ) {
                baos.write(buffer, 0, len);
            }
            baos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }


        // 打开一个新的输入流
        InputStream newInputStream = new ByteArrayInputStream(baos.toByteArray());
        return newInputStream;

    }




    public static void download(String url, String jsonString, File file, Header... headers) {
        InputStream in = null;
        FileOutputStream fout = null;
        HttpClient client = createClient(url);
        try {
            HttpPost httpPost = new HttpPost(url);
            if (headers != null) {
                for (Header each : headers) {
                    httpPost.setHeader(each);
                }
            }

            if (StringUtils.isNotEmpty(jsonString)) {
                StringEntity s = new StringEntity(jsonString, DEFAULT_ENCODING);
                s.setContentType(JSON_CONTENT_TYPE);
                httpPost.setEntity(s);
            }
            HttpResponse response = client.execute(httpPost);
            HttpEntity entity = response.getEntity();
            in = entity.getContent();
            fout = new FileOutputStream(file);
            int l = -1;
            byte[] tmp = new byte[4096];
            while ((l = in.read(tmp)) != -1) {
                fout.write(tmp, 0, l);
            }
            fout.flush();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        } finally {
            // 关闭低层流。
            try {
                fout.close();
            }catch (Exception e) {

            }
            try {
                in.close();
            }catch (Exception e) {

            }/*
            CloseUtils.close(fout);
            CloseUtils.close(in);*/
        }
    }


    public static <T> T get(String url,  final Class<T> resultClazz,
                            Map<String,String>  headerMap,Object param) {
        ResponseHandler<T> handler = new ResponseHandler<T>() {
            public T handleResponse(HttpResponse response)
                    throws ClientProtocolException, IOException {
                if (response.getStatusLine().getStatusCode() == 200) {
                    // http请求正常返回
                    HttpEntity httpEntity = response.getEntity();
                    T obj = JSONObject.parseObject(
                            EntityUtils.toString(httpEntity), resultClazz);
                    return obj;
                } else {
                    throw new IllegalStateException("post json bean fail:" + response.getStatusLine().getStatusCode());
                }
            }
        };
        return get(url, param, handler, headerMap);
    }

    public static <T> T get(String url, ResponseHandler<T> handler,
                            Header... header) {
        return get(url, null, handler, header);
    }

    public static <T> T get(String url, Object parameter, ResponseHandler<T> handler,
                            Header... header) {
        T res = null;

        HttpGet get = null;
        long startTime = 0L;
        try {
            //处理get  url数组拼装
            String param = "";
            List<NameValuePair> pairList = convertToNameValuePairList(parameter);
            if (!CollectionUtils.isEmpty(pairList)) {
                url = url + "?" + EntityUtils.toString(new UrlEncodedFormEntity(pairList, Consts.UTF_8));
                param = convertToArrayParam(parameter,"&");
            } else {
                param = convertToArrayParam(parameter,"?");
            }
            url = url +param;

            startTime = System.currentTimeMillis();
            HttpClient client = createClient(url);
            get = new HttpGet(url);
            if (header != null) {
                for (Header each : header) {
                    get.setHeader(each);
                }
            }

            res = client.execute(get, handler);
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        } finally {
            long endTime = System.currentTimeMillis();
            System.out.println("executeGetUrl:" + url + ",time:" + (endTime - startTime));
            if (get != null) {
                get.releaseConnection();
            }
        }
        return res;
    }

    public static <T> T get(String url, Object parameter, ResponseHandler<T> handler,
                            Map<String,String> headerMap) {
        T res = null;

        HttpGet get = null;
        long startTime = 0L;
        try {
            //处理get  url数组拼装
            String param = "";
            List<NameValuePair> pairList = convertToNameValuePairList(parameter);
            if (!CollectionUtils.isEmpty(pairList)) {
                url = url + "?" + EntityUtils.toString(new UrlEncodedFormEntity(pairList, Consts.UTF_8));
                param = convertToArrayParam(parameter,"&");
            } else {
                param = convertToArrayParam(parameter,"?");
            }
            url = url +param;

            startTime = System.currentTimeMillis();
            HttpClient client = createClient(url);
            get = new HttpGet(url);
            if (headerMap != null && headerMap.size() >0) {
                for (Map.Entry<String, String> temp : headerMap.entrySet() ) {
                    //循环map里面的每一对键值对，然后获取key和value即时想要的参数的 key和value
                    get.setHeader(temp.getKey(),temp.getValue());
                }
            }

            res = client.execute(get, handler);
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        } finally {
            long endTime = System.currentTimeMillis();
            System.out.println("executeGetUrl:" + url + ",time:" + (endTime - startTime));
            if (get != null) {
                get.releaseConnection();
            }
        }
        return res;
    }



    private static List<NameValuePair> convertToNameValuePairList(Object parameter) {
        List<NameValuePair> pairList = new ArrayList<>();
        if (parameter != null) {
            Field[] fields = FieldUtils.getAllFields(parameter.getClass());
            for (Field field : fields) {
                if ("serialVersionUID".equals(field.getName())) {
                    continue;
                }

                Object value = PropertyUtils.getProperty(parameter, field.getName());
                if (value != null) {
                    pairList.add(new BasicNameValuePair(field.getName(), String.valueOf(value)));
                }

            }
        }
        return pairList;
    }

    private static String convertToArrayParam(Object parameter,String spacer) {
        String param = "";
        if (parameter != null) {
            Field[] fields = FieldUtils.getAllFields(parameter.getClass());
            for (Field field : fields) {
                if ("serialVersionUID".equals(field.getName())) {
                    continue;
                }
                Class<?> type = field.getType();
                if (type.isArray()) {
                    String ids[] = (String[])PropertyUtils.getProperty(parameter, field.getName());
                    if(ids.length > 0){
                        for(int i = 0;i<ids.length;i++) {
                            param = StringUtils.join(param,"ids[]=", ids[i],"&");
                        }
                        param = StringUtils.join(spacer,param);
                        spacer = "&";
                    }
                }
            }
        }
        return param;
    }



   /* public static <T> T postJson(String url, ResponseHandler<T> handler) {
        return postJson(url, null, handler, null);
    }

    public static <T> T postJson(String url, String jsonString,
                                 ResponseHandler<T> handler) {
        return postJson(url, jsonString, handler, null);
    }*/

    public static <T> T postJson(String url, ResponseHandler<T> handler,
                                 Header... header) {
        return postJson(url, null, handler, header);
    }




    public static <T> T postJsonBean(String url, Object param, final Class<T> resultClazz,
                                     Header... header) {
        String jsonString = JSONObject.toJSONString(param);
        ResponseHandler<T> handler = new ResponseHandler<T>() {
            public T handleResponse(HttpResponse response)
                    throws ClientProtocolException, IOException {
                if (response.getStatusLine().getStatusCode() == 200) {
                    // http请求正常返回
                    HttpEntity httpEntity = response.getEntity();
                    String s = EntityUtils.toString(httpEntity);
                    System.out.println("返回结果："+s);
                    T obj = JSONObject.parseObject(
                            EntityUtils.toString(httpEntity), resultClazz);
                    return obj;
                } else {
                    throw new IllegalStateException("post json bean fail:" + response.getStatusLine().getStatusCode());
                }
            }
        };

        return postJson(url, jsonString, handler, header);
    }

    public static <T> T get(String url,  final Class<T> resultClazz,
                            Map<String,String>  headerMap) {
        ResponseHandler<T> handler = new ResponseHandler<T>() {
            public T handleResponse(HttpResponse response)
                    throws ClientProtocolException, IOException {
                if (response.getStatusLine().getStatusCode() == 200) {
                    // http请求正常返回
                    HttpEntity httpEntity = response.getEntity();
                    T obj = JSONObject.parseObject(
                            EntityUtils.toString(httpEntity), resultClazz);
                    return obj;
                } else {
                    throw new IllegalStateException("post json bean fail:" + response.getStatusLine().getStatusCode());
                }
            }
        };
        return get(url, null, handler, headerMap);
    }

    public static <T> T postJsonBean(String url, Object param, final Class<T> resultClazz,
                                     Map<String,String>  headerMap) {
        String jsonString = JSONObject.toJSONString(param);
        ResponseHandler<T> handler = new ResponseHandler<T>() {
            public T handleResponse(HttpResponse response)
                    throws ClientProtocolException, IOException {
                if (response.getStatusLine().getStatusCode() == 200) {
                    // http请求正常返回
                    HttpEntity httpEntity = response.getEntity();
                    T obj = JSONObject.parseObject(
                            EntityUtils.toString(httpEntity), resultClazz);
                    return obj;
                } else {
                    throw new IllegalStateException("post json bean fail:" + response.getStatusLine().getStatusCode());
                }
            }
        };

        return postJson(url, jsonString, handler, headerMap);
    }

    public static <T> T postJson(String url, String jsonString,
                                 ResponseHandler<T> handler, Map<String,String>  headerMap) {
        T res = null;
        long startTime = System.currentTimeMillis();
        long endTime = 0L;
        HttpClient client = createClient(url);
        HttpPost post = new HttpPost(url);

        try {
            if (headerMap != null && headerMap.size() >0) {
                for (Map.Entry<String, String> temp : headerMap.entrySet() ) {
                    //循环map里面的每一对键值对，然后获取key和value即时想要的参数的 key和value
                    post.setHeader(temp.getKey(),temp.getValue());
                }
            }

            if (StringUtils.isNotEmpty(jsonString)) {
                StringEntity s = new StringEntity(jsonString, DEFAULT_ENCODING);
                s.setContentType(JSON_CONTENT_TYPE);
                post.setEntity(s);
            }

            res = client.execute(post, handler);
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        } finally {
            endTime = System.currentTimeMillis();
            post.releaseConnection();

            System.out.println("postExecuteUrl:" + url + ",time:" + (endTime - startTime));
        }
        return res;
    }

    public static <T> T postJson(String url, String jsonString,
                                 ResponseHandler<T> handler, Header... header) {
        T res = null;
        long startTime = System.currentTimeMillis();
        long endTime = 0L;
        HttpClient client = createClient(url);
        HttpPost post = new HttpPost(url);

        try {
            if (header != null) {
                for (Header each : header) {
                    post.setHeader(each);
                }
            }

            if (StringUtils.isNotEmpty(jsonString)) {
                StringEntity s = new StringEntity(jsonString, DEFAULT_ENCODING);
                s.setContentType(JSON_CONTENT_TYPE);
                post.setEntity(s);
            }

            res = client.execute(post, handler);
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        } finally {
            endTime = System.currentTimeMillis();
            post.releaseConnection();

            System.out.println("postExecuteUrl:" + url + ",time:" + (endTime - startTime));
        }
        return res;
    }

    public static <T> T postForm(String url, Object parameter,
                                 Class<T> resultClazz, Header... header) {
        ResponseHandler<T> handler = new ResponseHandler<T>() {
            public T handleResponse(HttpResponse response)
                    throws ClientProtocolException, IOException {
                if (response.getStatusLine().getStatusCode() == 200) {
                    // http请求正常返回
                    HttpEntity httpEntity = response.getEntity();
                    T obj = JSONObject.parseObject(
                            EntityUtils.toString(httpEntity), resultClazz);
                    return obj;
                } else {
                    throw new IllegalStateException("post json bean fail:" + response.getStatusLine().getStatusCode());
                }
            }
        };

        T res = null;
        long startTime = System.currentTimeMillis();
        long endTime = 0L;
        HttpClient client = createClient(url);

        HttpPost post = new HttpPost(url);
        try {
            if (header != null) {
                for (Header each : header) {
                    post.setHeader(each);
                }
            }
            List<NameValuePair> pairList = convertToNameValuePairList(parameter);
            post.setEntity(new UrlEncodedFormEntity(pairList, Consts.UTF_8));

            res = client.execute(post, handler);
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalStateException(e);
        } finally {
            System.out.println("executePostFormUrl:" + url + ",time:" + (endTime - startTime));

            post.releaseConnection();
        }
        return res;
    }

    public static <T> T putJson(String url, String jsonString,
                                ResponseHandler<T> handler, Header... header) {
        T res = null;

        HttpClient client = createClient(url);

        HttpPut put = new HttpPut(url);
        try {
            if (header != null) {
                for (Header each : header) {
                    put.setHeader(each);
                }
            }

            if (StringUtils.isNotEmpty(jsonString)) {
                StringEntity s = new StringEntity(jsonString, DEFAULT_ENCODING);
                s.setContentType(JSON_CONTENT_TYPE);
                put.setEntity(s);
            }

            res = client.execute(put, handler);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        } finally {
            put.releaseConnection();
        }
        return res;
    }

    public static <T> T postXml(String url, String xmlString, String encoding,
                                ResponseHandler<T> handler) {
        return postXml(url, xmlString, encoding, handler, null);
    }

    public static <T> T postXml(String url, String xmlString, ResponseHandler<T> handler) {
        return postXml(url, xmlString, DEFAULT_ENCODING, handler, (Header[]) null);
    }

    public static <T> T postXml(String url, String xmlString, String encoding,
                                ResponseHandler<T> handler, Header... header) {
        T res = null;

        HttpClient client = createClient(url);

        HttpPost post = new HttpPost(url);
        try {
            if (header != null) {
                for (Header each : header) {
                    post.setHeader(each);
                }
            }

            if (StringUtils.isNotEmpty(xmlString)) {
                StringEntity s = new StringEntity(xmlString, encoding);
                s.setContentType(XML_CONTENT_TYPE);
                post.setEntity(s);
            }

            res = client.execute(post, handler);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        } finally {
            post.releaseConnection();
        }
        return res;
    }

    private static HttpClient createClient(String url) {
        if (client != null) {
            // 采用环境配置的httpClient实例
            return client;
        }

        // 创建HttpClient实例，注意这里创建的是非线程安全的
        HttpClient _client = null;
        if (url.startsWith("https")) {
            _client = createSSLClientDefault();
        } else if (url.startsWith("http")) {
            _client = HttpClients.createDefault();
        }
        return _client;
    }

    private static HttpClient createSSLClientDefault() {
        try {
            SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(
                    null, new TrustStrategy() {
                        // 信任所有
                        public boolean isTrusted(X509Certificate[] chain,
                                                 String authType) throws CertificateException {
                            return true;
                        }
                    }).build();
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                    sslContext);
            return HttpClients.custom().setSSLSocketFactory(sslsf).build();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public static void main(String[] args) {



    }
}