package com.ps.websocket.controller;

import com.ps.websocket.common.CommonConsumerMqReq;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/mq/send")
public class MqSendController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @PostMapping("")
    public String send(){
        CommonConsumerMqReq<List> req = new CommonConsumerMqReq<>();

        List<String> list = new ArrayList<>();
        list.add("哈哈");
        req.setData(list);

        //  发送消息，默认是持久化消息 里面实现 MessageDeliveryMode.PERSISTENT
        rabbitTemplate.convertAndSend("testDirectExchange","testDirectRouting",req);
        return "ok";
    }

    @PostMapping("/v2")
    public String sendV2(){
        CommonConsumerMqReq<List> req = new CommonConsumerMqReq<>();
        List<String> list = new ArrayList<>();
        list.add("呵呵");
        req.setData(list);
        //  发送消息，默认是持久化消息 里面实现 MessageDeliveryMode.PERSISTENT
        rabbitTemplate.convertAndSend("testDirectExchange2","testDirectRouting2",req);
        return "ok";
    }

    @PostMapping("/error")
    public String sendError(){
        rabbitTemplate.convertAndSend("testErrorDirectExchange","testDirectRouting","错误的直连交换机测试消息!");
        return "ok";
    }
}
