package com.ps.websocket.controller;

import com.ps.websocket.common.ServiceException;
import com.ps.websocket.domain.enumerate.PayEnum;
import com.ps.websocket.dto.PayReq;
import com.ps.websocket.service.PayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/pay")
@Slf4j
public class PayController {

    @Autowired
    private Map<PayEnum, PayService> payServiceMap;




    //支付接口
    @PostMapping("/toPay")
    public Object aliPay(@RequestBody PayReq req) throws Exception{
        ServiceException.asserts(null == req || null == req.getPayType(),"参数有误");
        PayService payService = payServiceMap.get(req.getPayType());
        Object result  = payService.toPay();


        return result;
    }

    @PostMapping("/queryTrade")
    public Object query() throws Exception{
        PayService payService = payServiceMap.get(PayEnum.WX_PAY);
        Object result = payService.queryTrade();
        return result;
    }


    /**
     * 微信支付结果回调地址
     * @param request
     */
    @PostMapping(value = "/payNotify")
    public Object payNotify(HttpServletRequest request) throws Exception {
        PayService payService = payServiceMap.get(PayEnum.WX_PAY);

        Object map = payService.payNotify(request);

        return map;
    }

    /**
     * 支付宝支付结果回调地址
     * @param request
     */
    @PostMapping(value = "/aliPayNotify")
    public Object aliPayNotify(HttpServletRequest request) throws Exception {
        PayService payService = payServiceMap.get(PayEnum.ALI_PAY);

        Object result = payService.payNotify(request);

        return result;
    }



}
