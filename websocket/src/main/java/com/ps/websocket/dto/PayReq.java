package com.ps.websocket.dto;

import com.ps.websocket.domain.enumerate.PayEnum;
import lombok.Data;

@Data
public class PayReq {

    private PayEnum payType;
}
