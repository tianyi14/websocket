package com.ps.websocket.dto;

import lombok.Data;

@Data
public class WxPayReq {

    private String appid;

    private String mchid;

    private String out_trade_no;

    private String notify_url;

    private String description;

    private PayAmountBean amount;

    @Data
    public static class PayAmountBean{
        private Integer total;

        private String currency;

    }

}
