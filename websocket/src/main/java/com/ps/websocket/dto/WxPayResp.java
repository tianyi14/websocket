package com.ps.websocket.dto;

import lombok.Data;

@Data
public class WxPayResp {


    private String appId;

    //商户号
    private String partnerId;

    //预支付ID
    private String prepayId;



    //随机字符串
    private String noncestr;

    //时间戳
    private String timestamp;

    //签名
    private String sign;

}
