package com.ps.websocket.dto.creator;

import com.ps.websocket.constant.PayConstants;
import com.ps.websocket.dto.WxPayResp;

public class WxPayRespCreator {

    public static WxPayResp cerate(String sign, String prepayId, String time, String nonce){
        WxPayResp resp = new WxPayResp();
        resp.setAppId(PayConstants.wxAppid);
        resp.setPartnerId(PayConstants.wxMchid);
        resp.setNoncestr(nonce);
        resp.setPrepayId(prepayId);
        resp.setTimestamp(time);
        resp.setSign(sign);
        return  resp;
    }
}
