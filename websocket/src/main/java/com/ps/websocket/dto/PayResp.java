package com.ps.websocket.dto;

import lombok.Data;

@Data
public class PayResp {

    private WxPayResp wxPayResp;

    private String aliPayResp;
}
