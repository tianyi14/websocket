package com.ps.websocket.dto;

import lombok.Data;

@Data
public class WxPayQueryReq {

    private String mchid;
}
