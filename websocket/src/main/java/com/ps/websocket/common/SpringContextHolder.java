package com.ps.websocket.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 以静态变量保存Spring ApplicationContext, 可在任何代码任何地方任何时候取出ApplicaitonContext.
 * 
 */
@Service
@Lazy(false)
public class SpringContextHolder implements ApplicationContextAware, DisposableBean {

	private static ApplicationContext applicationContext = null;

	private static ThreadLocal<Map<String,String>> threadLocal = new ThreadLocal<>();

	private static Logger logger = LoggerFactory.getLogger(SpringContextHolder.class);


	/**
	 * 从静态变量applicationContext中取得Bean, 自动转型为所赋值对象的类型.
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getBean(String name) {
		return (T) applicationContext.getBean(name);
	}


	/**
	 * 从静态变量applicationContext中取得Bean, 自动转型为所赋值对象的类型.
	 */
	public static <T> T getBean(Class<T> requiredType) {
		return applicationContext.getBean(requiredType);
	}

	/**
	 * 清除SpringContextHolder中的ApplicationContext为Null.
	 */
	public static void clearHolder() {
		if (logger.isDebugEnabled()) {
			logger.debug("清除SpringContextHolder中的ApplicationContext:" + applicationContext);
		}
		applicationContext = null;
	}

	/**
	 * 实现ApplicationContextAware接口, 注入Context到静态变量中.
	 */
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		SpringContextHolder.applicationContext = applicationContext;
	}

	/**
	 * 实现DisposableBean接口, 在Context关闭时清理静态变量.
	 */
	@Override
	public void destroy() throws Exception {
		SpringContextHolder.clearHolder();
	}


	/**
	 * 向自定义的线程私有上下文设置信息
	 */
	public static void setThreadBean(String key,String value) {
		Map<String, String> map = threadLocal.get();
		if (null == map) {
			map = new HashMap<>();
		}
		map.put(key,value);
		threadLocal.set(map);
	}

	/**
	 *  获取自定义的线程私有上下文信息
	 */
	public static String getThreadBean(String key) {
		Map<String, String> map = threadLocal.get();
		if (null == map) {
			map = new HashMap<>();
		}
 		return map.get(key);
	}

	/*
	* 清空 自定义线程上下文
	* */
	public static void removeThreadBean(){
		threadLocal.remove();
	}
}