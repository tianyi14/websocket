package com.ps.websocket.common;

@SuppressWarnings("serial")
public class ServiceException extends RuntimeException {
	private String code;

	public ServiceException(Throwable cause) {
		super(cause);
	}

	public ServiceException(String message) {
		this(message, "");
	}

	public ServiceException(String message, String code) {
		super(message);
		this.code = code;
	}

	public static void asserts(boolean state, String message) {
		AssertUtil.service(state, message);
	}

	public static void asserts(boolean state, String message, String code) {
		AssertUtil.service(state, message, code);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
