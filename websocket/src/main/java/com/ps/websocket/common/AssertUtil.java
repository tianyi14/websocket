package com.ps.websocket.common;

public class AssertUtil {
	/***
	 * 将抛出ServiceException
	 * 
	 * @param state
	 * @param message
	 * @throws ServiceException
	 */
	public static void service(String message) throws ServiceException {
		service(true, message);
	}

	/***
	 * 如果为true 将抛出ServiceException
	 * 
	 * @param state
	 * @param message
	 * @throws ServiceException
	 */
	public static void service(boolean state, String message) throws ServiceException {
		service(state, message, "");
	}

	/***
	 * 如果为true 将抛出ServiceException
	 * 
	 * @param state
	 * @param message
	 * @param input
	 * @param output
	 * @throws ServiceException
	 */
	public static void service(boolean state, String message, String code) throws ServiceException {
		if (state) {
			throw new ServiceException(message, code);
		}
	}

}
