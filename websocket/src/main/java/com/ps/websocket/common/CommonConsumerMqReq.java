package com.ps.websocket.common;

import lombok.Data;

import java.io.Serializable;
import java.util.UUID;

@Data
public class CommonConsumerMqReq<T> implements Serializable {

    public String uuid =  UUID.randomUUID().toString().replace("-","");

    private T data;
}
