package com.ps.websocket.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.ps.websocket.constant.PayConstants;
import com.ps.websocket.constant.RedisConstants;
import com.ps.websocket.domain.NotifyResourceVO;
import com.ps.websocket.domain.PayNotifyVO;
import com.ps.websocket.domain.enumerate.PayEnum;
import com.ps.websocket.dto.PayResp;
import com.ps.websocket.dto.WxPayQueryReq;
import com.ps.websocket.dto.WxPayReq;
import com.ps.websocket.dto.WxPayResp;
import com.ps.websocket.dto.creator.WxPayRespCreator;
import com.ps.websocket.service.PayService;
import com.ps.websocket.util.HttpUtils;
import com.ps.websocket.util.JedisUtils;
import com.ps.websocket.util.PayUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.ParseException;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.Signature;
import java.security.cert.Certificate;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class WxPayServiceImpl implements PayService {



    @Override
    public Object toPay() throws Exception{
        String orderNo = PayUtils.makeOrderNum();
        //构建参数 todo: 假的，商品信息后面肯定要让前端传的  后续还会加订单逻辑等
        WxPayReq wxPayReq = new WxPayReq();
        wxPayReq.setAppid(PayConstants.wxAppid);
        wxPayReq.setMchid(PayConstants.wxMchid);
        wxPayReq.setOut_trade_no(orderNo);
        wxPayReq.setNotify_url(PayConstants.wxNotifyurl);
        wxPayReq.setDescription("商品信息1号");
        WxPayReq.PayAmountBean amountBean = new WxPayReq.PayAmountBean();
        amountBean.setTotal(1);
        amountBean.setCurrency("CNY");
        wxPayReq.setAmount(amountBean);
        //构建header 签名等
        Map<String,String>  headerMap = new HashMap<>();
        log.info("支付参数:"+JSONObject.toJSONString(wxPayReq));

        headerMap.put("Authorization",PayConstants.WX_SIGN_HEADER_INFO + PayUtils.getToken("POST",PayConstants.WX_APP_CREATE_ORDER_URL, JSONObject.toJSONString(wxPayReq)));
        headerMap.put("Content-Type", "application/json");
        headerMap.put("Accept", "application/json");

        String result = HttpUtils.postJsonBean(PayConstants.WX_PAY_DOMAIN_URL+PayConstants.WX_APP_CREATE_ORDER_URL,
                wxPayReq,String.class,headerMap);
        JSONObject object = JSONObject.parseObject(result);
        String prepay_id = object.getString("prepay_id");
        //todo:prepayid 获取需要留存到订单表

        //生成二次签名   todo:后续如果有需求 可以加个二次签名的接口 让前端重新唤起支付
        //随机串
        String nonceStr = PayUtils.getRandomString(32);
        long timestamp = System.currentTimeMillis() / 1000;
        String secondSign = PayUtils.getSecondSign(prepay_id,nonceStr,timestamp);

        WxPayResp resp = WxPayRespCreator.cerate(secondSign, prepay_id, String.valueOf(timestamp), nonceStr);
        PayResp payResp = new PayResp();
        payResp.setWxPayResp(resp);
        return payResp;
    }


    @Override
    public Map<String,String> payNotify(HttpServletRequest request) throws Exception{
        Map<String,String> map = new HashMap<>(2);
        //todo:幂等处理多次回调情况
        //微信返回的请求体
        String body = getRequestBody(request);
        //如果验证签名序列号通过
        if (verifiedSign(request,body)){
            //微信支付通知实体类
            PayNotifyVO payNotifyVO = JSONObject.parseObject(body, PayNotifyVO.class);
            //如果支付成功
            if ("TRANSACTION.SUCCESS".equals(payNotifyVO.getEvent_type())){
                //通知资源数据
                PayNotifyVO.Resource resource = payNotifyVO.getResource();
                //解密后资源数据
                String notifyResourceStr = PayUtils.decryptResponseBody(resource.getAssociated_data(),resource.getNonce(),resource.getCiphertext());
                //通知资源数据对象
                NotifyResourceVO notifyResourceVO = JSONObject.parseObject(notifyResourceStr, NotifyResourceVO.class);
                //查询订单根据out_trade_no
                String outTradeNo = notifyResourceVO.getOut_trade_no();
                String trade_state = notifyResourceVO.getTrade_state();

                //todo:这里可以处理修改订单状态了 等等信息
                boolean lock = JedisUtils.lock(RedisConstants.PAY_LOCK_KEY_PRE + "wx:" + outTradeNo, 60000);
                if (lock) {
                    //todo:这里加判断 查询当前订单是否是未支付，如果不是，代表已经处理过支付完成状态，就不走下面逻辑了


                    JedisUtils.unlock(RedisConstants.PAY_LOCK_KEY_PRE + "wx:" + outTradeNo);

                }
                log.info("回调微信返回信息："+JSONObject.toJSONString(notifyResourceVO));

            }else{
                log.info("微信返回支付错误摘要："+payNotifyVO.getSummary());
            }
            //通知微信正常接收到消息,否则微信会轮询该接口
            map.put("code","SUCCESS");
            map.put("message","");
            return map;
        }

        return null;
    }


    /**
     * 验证微信签名
     * @param request
     * @param body
     * @return
     * @throws GeneralSecurityException
     * @throws IOException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws ParseException
     */
    private boolean verifiedSign(HttpServletRequest request,String body) throws GeneralSecurityException, ParseException, java.text.ParseException, UnsupportedEncodingException {
        //微信返回的证书序列号
        String serialNo = request.getHeader("Wechatpay-Serial");
        //微信返回的随机字符串
        String nonceStr = request.getHeader("Wechatpay-Nonce");
        //微信返回的时间戳
        String timestamp = request.getHeader("Wechatpay-Timestamp");
        //微信返回的签名
        String wechatSign = request.getHeader("Wechatpay-Signature");
        //组装签名字符串
        String signStr = Stream.of(timestamp, nonceStr, body)
                .collect(Collectors.joining("\n", "", "\n"));
        //当证书容器为空 或者 响应提供的证书序列号不在容器中时  就应该刷新了
        if (PayUtils.CERTIFICATE_MAP.isEmpty() || !PayUtils.CERTIFICATE_MAP.containsKey(serialNo)) {
            PayUtils.refreshCertificate();
        }
        //根据序列号获取平台证书
        Certificate certificate = PayUtils.CERTIFICATE_MAP.get(serialNo);
        //获取失败 验证失败
        if (certificate == null){
            return false;
        }
        //SHA256withRSA签名
        Signature signature = Signature.getInstance("SHA256withRSA");
        signature.initVerify(certificate);
        signature.update(signStr.getBytes());
        //返回验签结果
        return signature.verify(Base64Utils.decodeFromString(wechatSign));
    }


    public static void main(String[] args) {
        //组装签名字符串
        String signStr = Stream.of("11111111", "随机串", "报文")
                .collect(Collectors.joining("\n", "", "\n"));
        System.out.println(signStr);
    }




    /**
     * 获取请求文体
     * @param request
     * @return
     * @throws IOException
     */
    public static String getRequestBody(HttpServletRequest request) throws IOException {
        ServletInputStream stream = null;
        BufferedReader reader = null;
        StringBuffer sb = new StringBuffer();
        try {
            stream = request.getInputStream();
            // 获取响应
            reader = new BufferedReader(new InputStreamReader(stream));
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            throw new IOException("读取返回支付接口数据流出现异常！");
        } finally {
            reader.close();
        }
        return sb.toString();
    }

    @Override
    public PayEnum getKey() {
        return PayEnum.WX_PAY;
    }

    @Override
    public Object queryTrade() throws Exception {
        WxPayQueryReq req = new WxPayQueryReq();
        req.setMchid(PayConstants.wxMchid);
        Map<String,String> headerMap = new HashMap<>();
        String urlEnd = String.format(PayConstants.WX_APP_QUERY_ORDER_URL, "PAYORDERNO-N20210906171229830000");

        headerMap.put("Authorization",PayConstants.WX_SIGN_HEADER_INFO + PayUtils.getToken("GET",urlEnd+"?mchid="+PayConstants.wxMchid, ""));
        headerMap.put("Content-Type", "application/json");
        headerMap.put("Accept", "application/json");
        String result = HttpUtils.get(PayConstants.WX_PAY_DOMAIN_URL + urlEnd, String.class, headerMap, req);

        return result;
    }


}
