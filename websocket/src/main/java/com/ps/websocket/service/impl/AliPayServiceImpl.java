package com.ps.websocket.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.alipay.api.response.AlipayTradeQueryResponse;
import com.ps.websocket.common.ServiceException;
import com.ps.websocket.constant.PayConstants;
import com.ps.websocket.constant.RedisConstants;
import com.ps.websocket.domain.enumerate.PayEnum;
import com.ps.websocket.dto.PayResp;
import com.ps.websocket.service.PayService;
import com.ps.websocket.util.EnvironmentUtils;
import com.ps.websocket.util.JedisUtils;
import com.ps.websocket.util.PayUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Service
@Slf4j
public class AliPayServiceImpl implements PayService {

    @Autowired(required = false)
    @Qualifier("alipayClient")
    private DefaultAlipayClient alipayClient;




    @Override
    public Object toPay() throws Exception {
        //实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
        AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
        request.setNotifyUrl(PayConstants.ALI_NOTIFY_URL);
        String orderNo = PayUtils.makeOrderNum();
        //SDK已经封装掉了公共参数，这里只需要传入业务参数。以下方法为sdk的model入参方式(model和biz_content同时存在的情况下取biz_content)。
        AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
        model.setBody("裴帅测试订单");
        model.setSubject("测试订单标题");
        model.setOutTradeNo(orderNo);
        model.setTimeoutExpress("30m");
        model.setTotalAmount ("0.01");
        //model.setProductCode("QUICK_MSECURITY_PAY");
        request.setBizModel(model);
        try  {
            //这里和普通的接口调用不同，使用的是sdkExecute

            AlipayTradeAppPayResponse response = alipayClient.sdkExecute(request);
            log.info("支付宝返回body:"+response.getBody());
            PayResp resp = new PayResp();
            resp.setAliPayResp(response.getBody());
            return resp;//就是orderString 可以直接给客户端请求，无需再做处理。
        }  catch (AlipayApiException e) {
            e.printStackTrace();
        }
        ServiceException.asserts(true,"支付宝调起支付失败");
        return null;
     }

    @Override
    public String payNotify(HttpServletRequest request) throws Exception {
        //todo:幂等处理多次回调情况
        //获取支付宝POST过来反馈信息
        Map<String,String> params = new HashMap<String,String>();
        Map requestParams = request.getParameterMap();
        for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext ();) {
            String name =  ( String )iter.next();
            String[] values = (String[])requestParams.get(name);
            String valueStr="";
            for(int i = 0;i < values.length; i++){
                valueStr = (i== values.length-1)?valueStr+values[i]:valueStr+values[i] + ",";
            }
            //乱码解决，这段代码在出现乱码时使用。
            //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
            params.put(name,valueStr);
        }
        log.info("接收到支付宝回调信息："+JSONObject.toJSONString(params));
        //切记alipayPublicCertPath是支付宝公钥证书路径，请去open.alipay.com对应应用下载
        //验签结果
        boolean flag = AlipaySignature.rsaCertCheckV1(params, EnvironmentUtils.getString("alipay.cert.public.location"),"UTF-8","RSA2");
        if (flag) {
            //校验返回参数是否正确
            // out_trade_no 是否为商户系统中创建的订单号
            // 判断 total_amount 是否确实为该订单的实际金额（即商户订单创建时的金额），
            //  app_id 是否为该商户本身
            //校验通知中的 seller_id（或者 seller_email ) 是否为 out_trade_no 这笔单据的对应的操作方 (这个应该没有)

            //校验成功后 处理 逻辑  todo:注意 这里需要留存支付宝的trade_no 字段
            String tradeStatus = params.get("trade_status");
            String outTradeNo = params.get("out_trade_no");
            if ("TRADE_SUCCESS".equals(tradeStatus) || "TRADE_FINISHED".equals(tradeStatus)) {
                //付款成功处理  目前不支持退款 所以不需考虑退款后的  TRADE_FINISHED情况
                boolean lock = JedisUtils.lock(RedisConstants.PAY_LOCK_KEY_PRE + "ali:" + outTradeNo, 60000);
                if (lock) {
                    //  TODO:这里加判断 查询当前订单是否是未支付，如果不是，代表已经处理过支付完成状态，就不走下面逻辑了



                    JedisUtils.unlock(RedisConstants.PAY_LOCK_KEY_PRE + "ali:" + outTradeNo);
                }

            }
            //其他暂不管


            return "success";
        } else {
            log.error("验签失败:param:"+JSONObject.toJSONString(params));
            return "failure";
        }
    }

    @Override
    public PayEnum getKey() {
        return PayEnum.ALI_PAY;
    }

    @Override
    public Object queryTrade() throws Exception{

        AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
        Map<String,String> map = new HashMap<>();
        map.put("out_trade_no","20210906145725450007");
        request.setBizContent(JSONObject.toJSONString(map));
        AlipayTradeQueryResponse response = alipayClient.certificateExecute(request);
        if(response.isSuccess()){
            return response.getBody();
        } else {
            log.info("查询支付宝订单失败："+response.getBody());
        }

        return null;
    }


}
