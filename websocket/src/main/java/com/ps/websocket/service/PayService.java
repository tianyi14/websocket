package com.ps.websocket.service;

import com.ps.websocket.domain.enumerate.PayEnum;

import javax.servlet.http.HttpServletRequest;

public interface PayService {


    Object toPay() throws Exception;

    Object payNotify(HttpServletRequest request)  throws Exception;

    PayEnum getKey();
    //查询 订单交易信息
    Object queryTrade() throws Exception;
}
